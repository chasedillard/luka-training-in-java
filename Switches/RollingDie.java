/*
Example:
    When you roll a die, you receiive a number of apples equal with the value of the die face.
    Rolling die - generating the face value - is done with random() method
        value 1  =  get 1 apple
        value 2  =  get 2 apples
        value 3  =  get 3 apples
        value 4  =  get 4 apples
        value 5  =  get 5 apples
        value 6  =  get 6 apples
 
 
*/
 
public class RollingDie {
    public static void main(String[] args) {
        //declare variables
        int faceValue, noApples;
        //generate faceValue randomly to have values between 1 and 6
        faceValue = (int)(Math.random() * 6) + 1;
        System.out.println("Face value is " + faceValue);
        switch(faceValue) {
            case 1:
                noApples = 1;
                break;
            case 2:
                noApples = 2;
                break;
            case 3:
                noApples = 3;
                break;
            case 4:
                noApples = 4;
                break;
            case 5:
                noApples = 5;
                break;
            case 6:
                noApples = 6;
                break;
            default:
                noApples = 0;
                break;
        }
        System.out.println("You get " + noApples + " apples");
        //ternary operator
        // (condition)?action(value)1:action(value)2
        //want to write apple for 1 and apples for more than 1
        System.out.println("You get " + noApples + ((noApples == 1) ? " apple" : " apples"));
        //alternative
        System.out.println("You get " + noApples + " apple" + ((noApples != 1) ? "s" : ""));
 
    }
}
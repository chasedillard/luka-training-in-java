public class IncrementDecrementOne {
    public static void main(String[] args) {
        int a, b, x;
        a = 4;
        b = 11;
        //increment a by 1
        //a=a+1
        //a++ or ++a is equialent with a=a+1
        //a++ post operation increment => first perform operations and then increment
        //++a pre operation increment => first increment and then perform operation
 
        System.out.println("Initial alue a = " + a);  //4
        System.out.println(a++);  //4
 
        System.out.println(a);  //5
 
        System.out.println("Initial value b= " + b);  //11
        System.out.println(b++);  //12
 
        System.out.println(b);  //12
 
        //before operation a=5, b=12
        x = a++ * b--;
        System.out.println("x = " + x);   //result 55
        System.out.println("a = " + a);   //6
        System.out.println("b = " + b);   //11
 
        x = ((3 * a-- - 2) + (b++ * b--)) / a;      //((3*5-2)+(12*12))/5
        System.out.println("x = " + x);  //31
        System.out.println("a= " + a);   //5
        System.out.println("b= " + b);   //11
    }
}
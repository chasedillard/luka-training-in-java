import java.util.Scanner;

public class Bars {
    static void printBar(int num, String name) {
        System.out.printf("\n%s\t", name);
        for (int i = 0; i <= num; i++) {
            System.out.printf("*");
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // create array of specific size
        System.out.printf("Size of datasheet (40 please) : ");
        int length = input.nextInt();

        // Apply numbers to array
        int array[] = new int[length];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("\nInput number : ");
            array[i] = input.nextInt();
        }

        // checks if any numbers are below 1 or above 4
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 1 || array[i] > 4) {
                System.out.println("Invalid number : " + array[i]);
                System.exit(0);
            }
        }

        int egypt, france, norway, germany;
        egypt = france = norway = germany = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 1) egypt++;
            if (array[i] == 2) france++;
            if (array[i] == 3) norway++;
            if (array[i] == 4) germany++;
        }

        printBar(egypt, "Egypt");
        printBar(france, "France");
        printBar(norway, "Norway");
        printBar(germany, "Germany");
        System.out.println("\n");
    }
}

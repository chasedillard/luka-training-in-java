import java.util.Scanner;

public class NamePostal {
    public void printInfo(String fname[], String lname[], int postal[]) {
        for (int i = 0; i < fname.length; i++) {
            System.out.println((i + 1) + " : " + fname[i] +
                "\t" + lname[i] + "\t" + postal[i]);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        NamePostal obj = new NamePostal();
        String fname[] = new String[25];
        String lname[] = new String[25];
        int array[] = new int[25];

        for (int i = 0; i < 25; i++) {
            System.out.print((i + 1) + ") \nFirst Name : ");
            fname[i] = input.nextLine();
            System.out.print((i + 1) + ") \nLast Name : ");
            lname[i] = input.nextLine();
        }

        for (int i = 0; i < 25; i++) {
            System.out.print("\nPostal code : ");
            array[i] = input.nextInt();
        }

        input.close();

        obj.printInfo(fname, lname, array);
    }
}
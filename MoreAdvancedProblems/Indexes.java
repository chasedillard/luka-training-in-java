public class Indexes {
    static void evenIndex(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0)
                System.out.printf("%d, ", array[i]);
        }
    }

    static void evenElements(int[] array) {
        System.out.println("\n");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0)
                System.out.printf("%d, ", array[i]);
        }
    }

    static void rev(int[] array) {
        System.out.println("\n");
        for (int i = array.length - 1; i >= 0; i--)
            System.out.printf("%d, ", array[i]);
    }

    static void firstAndLast(int[] array) {
        System.out.println("\n" + array[0] + ", " + array[array.length - 1]);
    }

    static void minAndMax(int[] array) {
        int min = array[0], max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) min = array[i];
            if (array[i] > max) max = array[i];
        }

        System.out.printf("Min : %d\nMax : %d\n", min, max);
    }

    static int sumOfElements(int[] array) {
        int a = array[0];
        for (int i = 1; i < array.length; i++)
            a += array[i];

        return a;
    }

    static int alternatingSum(int[] array) {
        int even = 0, odd = 0;
        for (int i = 0; i < array.length; i += 2)
            even += array[i];
        
        for (int i = 1; i < array.length; i += 2)
            odd += array[i];

        return even - odd;
    }

    public static void main(String[] args) {
        var a = new int[] { 1, 2, 3, 4 };
        evenIndex(a);
        evenElements(a);
        rev(a);
        firstAndLast(a);
        minAndMax(a);
        System.out.println(sumOfElements(a));
        System.out.println(alternatingSum(a));
    }
}
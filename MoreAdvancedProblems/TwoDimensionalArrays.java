public class TwoDimensionalArrays {
    public int getTotal(int[][] array) {
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; i++) {
                total += j;
            }
        }

        return total;
    }

    public int getAverage(int[][] array) {
        int totalValue = this.getTotal(array);
        int population = array[0].length * array.length;
        return totalValue / population;
    }

    public int getRowTotal(int[][] array) {
        return array.length;
    }

    public int getColumnTotal(int[][] array) {
        return array[0].length;
    }

    public int getHighestInRow(int[][] array, int x) {
        int highest = array[x - 1][0];
        for (int i = 1; i < array[x - 1].length; i++) {
            if (highest < array[x - 1][i]) {
                highest = array[x - 1][i];
            }
        }

        return highest;
    }

    public int getHighestInColumn(int[][] array, int x) {
        int highest = array[0][x - 1];
        for (int i = 1; i < array.length; i++) {
            if (highest < array[i][x - 1]) {
                highest = array[i][x - 1];
            }
        }

        return highest;
    }

    public static void main(String[] args) {
        int[][] testData = new int[][] { { 10, 20, 30, 40, 50 },
                                         { 60, 70, 80, 90, 10 },
                                         { 12, 15, 16, 13, 19 },
                                         { 60, 45, 34, 93, 17 } };
            
        TwoDimensionalArrays obj = new TwoDimensionalArrays();
        System.out.println(obj.getHighestInColumn(testData, 1));
    }
}
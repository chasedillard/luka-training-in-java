/*
 * This is using if-else statements in order to get the amount of money that 
 * someone would have to pay considering their wage. 
 */

public class IncomeTax {
    public static void main(String[] args) {
        // create income and tax variables
        int tax, income = 40000;

        // checks how much your income is and assigns the correct tax
        if (income <= 50000) {
            tax = 1;
        } else if (income <= 75000) {
            tax = 2;
        } else if (income <= 100000) {
            tax = 3;
        } else if (income <= 250000) {
            tax = 4;
        } else if (income <= 500000) {
            tax = 5;
        } else {
            tax = 6;
        }

        // prints result
        System.out.println("Your tax is: " + tax);
    }
}
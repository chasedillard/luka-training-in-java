/*
 * Example of an if-else statement.
 */

public class ExOne {
    public static void main(String[] args) {
        boolean conditionOne, conditionTwo;
        conditionOne = conditionTwo = true;

        // if these two variables equal each other then the code
        // within the braces will run.
        if (conditionOne == conditionTwo) {
            System.out.println("They are true!");
        }

        // after that if statement has finished the rest
        // of the program continues.
        System.out.println("Outside of the loop...");
    }
}
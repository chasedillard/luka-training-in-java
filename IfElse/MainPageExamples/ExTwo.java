/*
 * Example Two of if-else statements
 */

public class ExTwo {
    public static void main(String[] args) {
        int example = 1;

        // this part of the if-else statement should not
        // run, because it's false.
        if (example == 0) {
            System.out.println("Zero!");
        }

        // this part, however, will run, as it evaluates to 
        // true. It is a continuation of the if-statement above.
        else if (example == 1) {
            System.out.println("One!");
        }

        // afterwards, the rest of the program will run.
        System.out.println("Rest of the Program!");
    }
}
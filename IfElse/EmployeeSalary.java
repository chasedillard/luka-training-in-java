/*
Write a program that reads in the name and salary of an employee. Here the salary will denote an hourly wage, such as $9.25. Then ask how many hours the employee worked in the past week. Be sure to accept fractional hours. Compute the pay. Any overtime work (over 40 hours per week) is paid at 150 percent of the regular wage. Print a paycheck for the employee.
*/
 
import java.util.Scanner;

public class EmployeeSalary {
    public static void main(String[] args) {
 
        Scanner input = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String name = input.nextLine();

        System.out.print("Enter your hourly wage: ");
        Double wage = input.nextDouble();

        System.out.print("How many hours have you worked this past week? ");
        Double hours = input.nextDouble();
 
        Double salary, salaryOver;

        if (hours > 40) {
            salaryOver = ((wage * 1.5) * (hours - 40)) + (40 * wage);
            System.out.print("Your paycheck this week is $" + salaryOver);
        } else {
            salary = (wage * hours);
            System.out.print("Your paycheck this week is $" + salary);
        }
    }
}
/*
Loops fit for applications when we do not know the number of repetitions
While loops
    *Pre-test while loops
        initialize condition variable
        while(condition)
            actions
            modify condition variable
        Example: calculate the sum of first 5 natural numbers
        Java implementation
            int sum = 0;
            int k = 1;
            while(k< = 5)
            {
                sum = sum + k;
                k = k + 1;
            }
 
    *Post-test while loops
        initialize condition variable
        do
            actions
            modify condition variable
        while(condition)
        Example: calculate the sum of first 5 natural numbers
        Java implementation
            int sum = 0;
            int k = 1;
            do
            {
                sum = sum + k;
                k = k + 1;
            }
            while(k< = 5);
 
    *Loops that have known number of repetitions
        Example: calculate sum of first 5 natural numbers
        int sum = 0;
        for(int k = 1;k< = 5;k +  + )
        {
            sum = sum + k;
        }
 
*/
 
public class LoopExercises {
    public static void main(String[] args) {
        //pre-test while loop
        int sum = 0;
        int k = 1;
        while(k <= 5) {
            sum = sum + k;
            System.out.println("Sum = " + sum);
            k = k + 1;
        }
        System.out.println("Final result for pre-test sum is: " + sum);
 
 
        //post-test while loop
        sum = 0;
        k = 1;
        do {
            sum = sum + k;
            System.out.println("Sum = " + sum);
            k = k + 1;
        } while(k <= 5);
        System.out.println("Final result for post-test sum is: " + sum);
 
        //for loop
        sum = 0;
        for(int i = 1; i< = 5; i +  + ) {
            sum = sum + i;
            System.out.println("Sum = " + sum);
        }
        System.out.println("Final result using for loop sum is: " + sum);
    }
}
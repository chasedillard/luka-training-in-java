/* 
 * This program takes three floating-point numbers and gives you statstics
 * on them. It gives the average, the smallest, and the largest.
 */

 import java.util.Scanner;

public class FloatCalculation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double a, b, c;

        System.out.println("Give three numbers: ");
        a = input.nextDouble();
        b = input.nextDouble();
        c = input.nextDouble();

        // makes an array in order to utilize a for loop
        double[] numArray = new double[] { a, b, c };
        double biggest = numArray[0];
        for (int i = 1; i < numArray.length; i++) {
            if (numArray[i] > biggest) {
                biggest = numArray[i];
            }
        }

        double smallest = numArray[0];
        for (int i = 1; i < numArray.length; i++) {
            if (numArray[i] < smallest) {
                smallest = numArray[i];
            }
        }

        double average = numArray[0];
        for (int i = 1; i < numArray.length; i++) {
            average += numArray[i];
        } average /= numArray.length;

        System.out.println("Biggest: " + biggest);
        System.out.println("Smallest: " + smallest);
        System.out.println("Average: " + average);
    }
}
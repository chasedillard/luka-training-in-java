/*
 * Creates a loop that continues indefinitely until a certain condition is not met.
 * Any loop-like thing can be created with a 'while' loop.
 */

public class WhileLoop {
    public static void main(String[] args) {
        // creates a variable called 'counter' that is used in the 
        // while loop. It will be used to count to 100.
        int counter = 0;

        // creates a 'while' loop that will keep counting until the number
        // 100 is reached.

        /*
         * as long as the statement within the parentheses evaluate to true
         * (a.k.a. if the counter variable is, in fact, less than or equal to 
         * 100, the while loop will run all the code within the braces at least
         * one more time.
         */
        while (counter <= 100) {
            // prints the what the value of counter is.
            System.out.println(counter);

            // adds one to the counter variable.
            counter++;
            /*
             * ^ this notation may look weird, but adding the '++' operator
             * will add one to whatever variable it's attached to.
             * The other operator like this is '--', which subtracts one
             * from the variable it's attached to.
             */
        }

        // code resumes after the while loop stops iterating (running multiple times).
        System.out.println("Rest of the code.");
    }
}
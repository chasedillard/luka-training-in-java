/* 
 * This program is designed to take a string and print each char of it
 * on a new line
 */

public class LinePrint {
    public static void main(String[] args) {
        String name = "Harry";

        // a string is actually an array of single characters.
        // i treated it like one with this
        for (int i = 0; i < name.length(); i++) {
            System.out.println(name.charAt(i));
        }
    }
}
public class Handshakes {
    public static void main(String[] args) {
        int nPeople = 100;   //number people expected at party
        //  int counter = 0;   //counter monitors people as they arrive
        int handShakes = 0;   //number of handshakes that occur
        for(int counter = 1; counter <= nPeople; counter++) {
            handShakes = handShakes + counter - 1;
            System.out.println("Current number of handshakes: " + handShakes);
        }

        System.out.println("In a party with " + nPeople + " people, there are " + handShakes + " handshakes!");  //4950 handshakes
    }
}
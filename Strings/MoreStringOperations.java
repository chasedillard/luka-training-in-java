import java.util.Scanner;

public class MoreStringOperations {
    public static void main(String[] args) {
        //declare and instantiate object scanner
        Scanner input = new Scanner(System.in);
        //prompt user to enter a string
        System.out.print("Enter a string: ");
        //declare and read the string from the user
        String first = input.nextLine();
        //print text entered by user
        System.out.println(first);
 
        //prompt user to enter the second string
        System.out.print("Enter a second string: ");
        String second = input.nextLine();
        System.out.println("Initial strings are: ");
        System.out.println(first);
        System.out.println(second);
        //concatonate and display the two strings using + operator
        System.out.println("Concatonated strings are: ");
        System.out.println(first+second);
 
        System.out.println("Concatonation using concat method: ");
        System.out.println(first.concat(second));
        System.out.println("Concatonation in reverse order - second followed by first: ");
        System.out.println(second.concat(first));
 
        //use concat method to put together first string, comma and space, followed by second string
        System.out.println("Concat method with a comma and space before 2nd string: ");
        System.out.println(first.concat(", ").concat(second));
 
        //change the letters of first string to all upper case
        System.out.println("Method applying upper case to first string: ");
        System.out.println(first.toUpperCase());
 
        //change the letters of second string to all lower case
        System.out.println("Method applying lower case to second string: ");
        System.out.println(second.toLowerCase());
 
        /*scramble your messages by creating a new string, which you declare as mixed, in which you concatonate first part of first followed by second part of second followed by first part of second, and second part of first
        */
        String mixt;
        mixt = first.substring(0,first.length() / 2) + second.substring(second.length() / 2) + second.substring(0,second.length() / 2) +
        first.substring(first.length() / 2);
        System.out.println("Scrambled messages are: " + mixt);
 
        System.out.println("Use concat for the previous operation: ");
        String mixtConcat = first.substring(0,first.length() / 2).concat(second.substring(second.length() / 2)).concat(second.substring(0,second.length() / 2)).concat(first.substring(first.length() / 2));
        System.out.println("Concat method scrambed messages are: " + mixt);
 
        //test method equals
        //print the true value given by method equals
        System.out.println("Scrambled strings through both methods statements is: " + mixt.equals(mixtConcat));
 
        //compare the two initial strings and display result
        System.out.println("First string compared with second: " + first.compareTo(second));
        System.out.println("Second string compared with first: " + second.compareTo(first));
 
        //you can find any character at a given index with charAt();
        //display the first character in first, and last character in second
        System.out.println("First string starts with: " +first.charAt(0));
        System.out.println("Second string ends with: " +second.charAt(second.length()-1));
 
        //find a string within another string
        System.out.println(second + " is in " + first + " at position " + first.indexOf(second));
        //if it gives negative, that means second string is not part of first string because negatives aren't possible in index
 
        //create a new string named missing, in which you take out the second string from first, assuming that second is part of first
        String missing = first.substring(0, first.indexOf(second)) + first.substring((first.indexOf(second) + second.length()));
        System.out.println("First string without second string is: " + missing);
    }
}
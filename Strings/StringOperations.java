public class StringOperations {
    public static void main(String[] args) {
        String message;
        message = "Hello";
 
        String name = "Brenda";
 
        String greeting = message + name;
        //if I want output "Hello, Brenda!"
        greeting  =  message + ", " + name + "!";
        System.out.println(greeting);
        int age = 54;
        int life = 20;
        System.out.print("Age in 20 years: ");
        System.out.println(age + life);  //74
        System.out.println("Age in 20 years: " + age + life);  //5420
        System.out.println("Correct age in 20 years: " + (age + life)); //74
 
    //index starts with 0
    //if length of string is 10, last index is 9
    //carAt(int index) returns character at index
        System.out.println("Third character of message: " + message.charAt(2));
        System.out.println("Length of greeting: " + greeting.length());
        System.out.println("Last character of name: " + name.charAt(name.length() - 1));
 
    }
}
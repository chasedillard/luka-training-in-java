/*
 * This example displays adding strings together along with variables. This
 * kind of string manipulation can be very helpful if you want to print the contents
 * of a variable within a string in order to explain the variable more clearly.
 *
 * Example statement (WHAT NOT TO DO): How old are you? 
 *                             OUTPUT: 17
 *
 * Example statement (WHAT TO DO): How old are you?
 *                         OUTPUT: I am 17 years old
 */

public class ExOne {
    public static void main(String[] args) {
        // create two separate strings to add together
        String firstName = "Chase", lastName = "Dillard";

        // create another string that equals the first and last
        // names combined (with a space as well)
        String fullName = firstName + " " + lastName;

        // print the full name
        System.out.println(fullname);
    }
}
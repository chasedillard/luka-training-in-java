public class Hangman {
    public String secretWord = "magic";
    public char disguisedWord[] = "?????".toCharArray();
    public int guesses = 0, wrongs = 0;

    /** Creates a temporary string equal to disguised word, then replaces disguised word chars
     * if the guess is correct. If there is no change in the disguised word, wrongs increment one,
     * and if the disguised word was guessed it prints "you've guessed it!". Compared to the original design,
     * I would also add a checker inside this method to prevent it from running if you guessed the word right.
     * @param letter
     */
    public void makeGuess(char letter) {
        char tmpWord[] = disguisedWord;

        for (int i = 0; i < secretWord.length(); i++) {
            if (letter == secretWord.charAt(i))
                disguisedWord[i] = letter;
        }

        if (tmpWord == disguisedWord) {
            wrongs++;
        } else if (disguisedWord.toString() == secretWord) {
            System.out.println("You've guessed it!");
        }

        guesses++;
    }

    /** returns the secret word. */
    public String getSecretWord() {
        return secretWord;
    }

    /** returns the guess counter. */
    public int getGuessCount() {
        return guesses;
    }

    /** checks the disguised word to see if it matches the secret word. Prints true if so, and false if not. */
    public boolean isFound() {
        if (disguisedWord.toString() == secretWord) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Hangman game = new Hangman();
    }
}
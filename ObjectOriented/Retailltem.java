public class Retailltem {
    String description;
    int units;
    double price;

    public Retailltem(String desc, int units, double price) {
        this.description = desc;
        this.units = units;
        this.price = price;
    }

    public static void main(String[] args) {
        var A = new Retailltem("Jacket", 12, 59.95);
        var B = new Retailltem("Designer Jeans", 40, 34.95);
        var C = new Retailltem("Shirt", 20, 24.95);
    }
}
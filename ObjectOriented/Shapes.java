public class Shapes {
    private float height;
    private float radius;

    public Shapes(float height, float radius) {
        this.height = height;
        this.radius = radius;
    }

    public float getSurfaceArea() {
        return (2 * (float)Math.PI * this.radius * this.height) + 
            ((float)Math.PI * radius * radius);
    }

    public float getVolume() {
        return 2 * (float)Math.PI * this.height * this.radius * this.radius;
    }
}
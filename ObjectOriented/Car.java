public class Car {
    double efficiency;
    double tank;

    public Car(double efficiency) {
        this.efficiency = efficiency;
        this.tank = 0;
    }

    public void addGas(double num) {
        this.tank += num;
    }

    public void drive(double num) {
        this.tank -= num / this.efficiency;
    }

    public double getGasLevel() {
        return this.tank;
    }
}
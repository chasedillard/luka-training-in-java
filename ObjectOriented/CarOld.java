public class CarOld {
    //attributes
    private String make;
    private String model;
    private int year;
 
    //accessors
    public String getMake() {
        return make;
    }
    public String getModel() {
        return model;
    }
    public int getYear() {
        return year;
    }
 
    //mutators
    public void setCar(String make, String model, int year) {
        this.make = make;
        this.model = model;
        this.year = year;
        //this says that you're using the current object
    }
 
    public String toString() {
        return ("Car information is: " + "\nMake: " + make + "\nModel: " + model + "\nYear " + year);
    }
 
    //two cars are equal if they have same make and model
    public boolean equals(Car myCar) {
        return (make.equalsIgnoreCase(myCar.getMake())) && (model.equalsIgnoreCase(myCar.getModel()));
    }
}
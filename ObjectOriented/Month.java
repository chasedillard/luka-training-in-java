public class Month {
    private int monthNumber;

    public Month(int month) {
        monthNumber = month;
    }

    public Month() {
        monthNumber = 1;
    }

    // giant if string
    public Month(String input) {
        if (input.trim().toLowerCase().equals("january"))
            monthNumber = 1;
        else if (input.trim().toLowerCase().equals("february"))
            monthNumber = 2;
        else if (input.trim().toLowerCase().equals("march"))
            monthNumber = 3;
        else if (input.trim().toLowerCase().equals("april"))
            monthNumber = 4;
        else if (input.trim().toLowerCase().equals("may"))
            monthNumber = 5;
        else if (input.trim().toLowerCase().equals("june"))
            monthNumber = 6;
        else if (input.trim().toLowerCase().equals("july"))
            monthNumber = 7;
        else if (input.trim().toLowerCase().equals("august"))
            monthNumber = 8;
        else if (input.trim().toLowerCase().equals("september"))
            monthNumber = 9;
        else if (input.trim().toLowerCase().equals("october"))
            monthNumber = 10;
        else if (input.trim().toLowerCase().equals("november"))
            monthNumber = 11;
        else if (input.trim().toLowerCase().equals("december"))
            monthNumber = 12;
    }

    public void setMonthNumber(int x) {
        if (x < 1 || x > 12)
            this.monthNumber = 1;
        else
            this.monthNumber = x;
    }

    public int getMonthNumber() {
        return this.monthNumber;
    }

    public String getMonthName() {
        switch (this.monthNumber) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            default:
                return "December";
        }
    }

    public String toString() {
        switch (this.monthNumber) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            default:
                return "December";
        }
    }

    public boolean equals(int number) {
        if (number == this.monthNumber) return true;
        else return false;
    }

    public boolean greaterThan(int number) {
        if (this.monthNumber > number) return true;
        else return false;
    }

    public boolean lessThan(int number) {
        if (this.monthNumber < number) return true;
        else return false;
    }
}
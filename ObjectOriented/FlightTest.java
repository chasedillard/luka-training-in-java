/*
 * This flight program is designed to create a "map" of sorts to a flight. 
 * It'll contain information on everything having to do with the flight,
 * contained within an object. This object can then do stuff. The class is
 * flight, and any time you call flight, you create an *object* of it to 
 * use and manipulate. You can manipulate the object with the methods and
 * variables you add to the class.
 */

class Flight {
    // create variables for the Flight class and any object that it uses
    public String flightName, flightOrigin, flightDestination;
    public int flightNumber;

    // when you create a new flight object, you define all of the variables
    // with a *constructor*
    public Flight(String name, String origin, String destination, int number) {
        flightName = name;
        flightOrigin = origin;
        flightDestination = destination;
        flightNumber = number;
    }

    public String toString() {
            return "Name: " + flightName +
            "\nOrigin: " + flightOrigin + 
            "\nDestination: " + flightDestination +
            "\nNumber: " + flightNumber;
    }
}

public class FlightTest {
    public static void main(String[] args) {
        Flight a = new Flight("foo", "bar", "bizz", 17);
        System.out.println(a.toString());
    }
}
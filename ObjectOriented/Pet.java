/*
System/software design steps (waterfall model):
    1. Requirements Elicitation - Understanding the problem
    2. System Analysis - Modeling
        * UML (Unified Modeling Language) diagrams:
            - activity diagrams
            - use case diagrams
            - class diagrams
            - sequence diagrams
            - ERD (entity relationship diagrams)
    3. System Design - detailed modeling
        * Establish all software and hardware details, along with any API needed
    4. System Implementation - coding
        * Write all source code, compile, and test (at unit level)
    5. Testing
        * Test entire system from functional perspective
    6. Deployment
        * Implement the system at client location
    7. Maintenance and Review
        * Fix any bugs
        * Review the entire development process and create patterns
 
Object Oriented Programming (OOP) uses Unified Process Model - combines stages of Waterfall model (like requirements+modeling, modeling+design+implement, and so on)
 
 
Object  =  features + action
        =  attributes (variables) + methods
Class  =  blueprint of the objects of same type
 
Class Model
    ---------------------------------------------
    |   class name                               |
    ---------------------------------------------
    |   -variable1: type                        |
    |   -variable2: type                        |
    |-------------------------------------------|
    |   +method name (arguments): type          |
    |   +accessors (get methods): att type      |
    |   +mutators (set methods): void           |
    |   +toString(): String                     |
    |   +equals(type argument): boolean         |
    ---------------------------------------------
 
*Class allows instantiation of objects of a certain type using 'new' (operator for creating an object)
Example:    Scanner input  =  new Scanner(System.in);
            type    name    ^operator to create an object
                                ^constructor - has same name as the class
 
* for now, we do not define constructors, and Java will provide a default constructor that will instantiate objects with all numerical values 0 (zero), and all object type values null.
    - private   (used only by the class)    -> attributes
    + public    (available for use by other classes that use the particular class)  -> most methods (some methods can be private if used just by the class
 
 
    EXAMPLE: class Pet (first letter capital)
        ------------------------------------------------------------------------------------
        |   Pet                                                                             |
        ------------------------------------------------------------------------------------
        |   -species: String                                                                |
        |   -breed: String                                                                  |
        |   -age: int                                                                       |
        |   -weight: double                                                                 |
        |-----------------------------------------------------------------------------------|
        |   +getSpecies(): String                                                           |
        |   +getBreed(): String                                                             |
        |   +getAge(): int                                                                  |
        |   +getWeight(): double                                                            |
        |   +setBreed (String myBreed): void                                                |
        |   +setSpeciesBreed (String mySpecies, String myBreed): void                       |
        |   +setBreedAgeWeight (String myBreed, int myAge, double myWeight): void           |
        |   +setPet (String mySpecies, String myBreed, int myAge, double myWeight): void    |
        |   +toString(): String                                                             |
        |   +equals(type argument): boolean                                                 |
        -------------------------------------------------------------------------------------
 
*/
 
public class Pet {
    //attributes (very very rare for them to be public)
    private String species;
    private String breed;
    private int age;
    private double weight;
 
    //no constructor at this point
 
    //accessors
    public String getSpecies() { return species; }
 
    public String getBreed() { return breed; }
 
    public int getAge() { return age; }
 
    public double getWeight() { return weight; }
 
    //mutators
    public void setBreed (String myBreed) {
        breed   = myBreed;
    }
    public void setSpeciesBreed (String mySpecies, String myBreed) {
        species = mySpecies;
        breed   = myBreed;
    }
    public void setBreedAgeWeight (String myBreed, int myAge, double myWeight) {
        breed   = myBreed;
        age     = myAge;
        weight  = myWeight;
    }
    public void setPet (String mySpecies, String myBreed, int myAge, double myWeight) {
        species = mySpecies;
        breed   = myBreed;
        age     = myAge;
        weight  = myWeight;
    }
 
    public String toString() {
        String str = ("Species: " + species + "\nBreed: " + breed + "\nAge: " + age + "\nWeight: " + weight);
        return str;
    }
 
    //equals method for same species
    public boolean equals(String mySpecies) {
        if(species.equalsIgnoreCase(mySpecies)) {
            return true;
        } else {
            return false;
        }
    //same as writing: return (species.equalsIgnoreCase(mySpecies));
    }
 
    //two pets are the same if they have same species and breed
    public boolean equals(Pet myPet) {
        return ((species.equalsIgnoreCase(myPet.getSpecies())) && (breed.equalsIgnoreCase(myPet.getBreed())));
    }
   
}